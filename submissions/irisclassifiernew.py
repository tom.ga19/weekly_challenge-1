
# load the iris dataset as an example 
from sklearn.datasets import load_iris
import pandas as pd
 
iris = pd.read_csv("../data/iris.data")
iris.columns = ['sepal length', 'sepal width', 'petal length', 'petal width', 'species']

from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.metrics import classification_report
# store the feature matrix (X) and response vector (y)
X = iris.drop('species',axis=1)
y = iris['species']
print(type(X))
# splitting X and y into training and testing sets 
from sklearn.model_selection import train_test_split 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=1) 
  
# printing the shapes of the new X objects 
print(X_train.shape) 
print(X_test.shape) 
  
# printing the shapes of the new y objects 
print(y_train.shape) 
print(y_test.shape)

clf=tree.DecisionTreeClassifier()
clf.fit(X_train,y_train)

prediction=clf.predict(X_test)

print("predicted values=",prediction)
print("actual values=",y_test)


knn=KNeighborsClassifier(n_neighbors=3)
knn.fit(X_train,y_train)

tree_predict=clf.predict(X_test)
neighbor_predict=knn.predict(X_test)

print("Accuracy of the decision tree classifier=",metrics.accuracy_score(y_test,tree_predict))
print("classification report")
print(classification_report(y_test,tree_predict))
print("Accuracy of the K-Nearest Neighbor classifier=",metrics.accuracy_score(y_test,neighbor_predict))
print("classification report")
print(classification_report(y_test,neighbor_predict))

"""  OUTPUT
Accuracy of the decision tree classifier= 0.9555555555555556
classification report
                 precision    recall  f1-score   support

    Iris-setosa       1.00      1.00      1.00        16
Iris-versicolor       0.94      0.94      0.94        16
 Iris-virginica       0.92      0.92      0.92        13

       accuracy                           0.96        45
      macro avg       0.95      0.95      0.95        45
   weighted avg       0.96      0.96      0.96        45

Accuracy of the K-Nearest Neighbor classifier= 0.9555555555555556
classification report
                 precision    recall  f1-score   support

    Iris-setosa       1.00      1.00      1.00        16
Iris-versicolor       0.94      0.94      0.94        16
 Iris-virginica       0.92      0.92      0.92        13

       accuracy                           0.96        45
      macro avg       0.95      0.95      0.95        45
   weighted avg       0.96      0.96      0.96        45

"""
